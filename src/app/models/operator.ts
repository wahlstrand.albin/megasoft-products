export interface OperatorCountry {
    iso_code: string;
    name: string;
}

export interface Operator {
    country: OperatorCountry;
    id: number;
    name: string;
}