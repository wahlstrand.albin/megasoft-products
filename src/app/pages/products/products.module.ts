import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '@msoft/modules/material/material.module';
import { ProductModule } from '@msoft/modules/product/product.module';
import { ProductFilterModule } from '@msoft/modules/product-filter/product-filter.module';

import { ProductsComponent } from './products.component';

import { productsRoutes } from './products.routes';

@NgModule({
    declarations: [
        ProductsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        ProductModule,
        ProductFilterModule,
        RouterModule.forChild(productsRoutes)
    ]
})
export class ProductsModule { }
