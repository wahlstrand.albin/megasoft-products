import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Product } from '@msoft/models/product';
import { CountryService } from '@msoft/services/country/country.service';
import { SnackbarService } from '@msoft/services/snackbar/snackbar.service';

@Component({
    selector: 'msoft-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent {
    isExpanded = false;

    @Input() product: Product;

    constructor(
        private countryService: CountryService,
        private snackbarService: SnackbarService
    ) {}

    getCountryName() {
        return this.product?.operator?.country?.name;
    }

    getCountryFlag() {
        const countryCode = this.product?.operator?.country?.iso_code ?? '';

        return this.countryService.getCountryFlag(countryCode);
    }

    getOperatorName() {
        return this.product.operator?.name;
    }

    getValidityText() {
        const unitSuffix = this.product.validity.quantity > 1 ? 's' : '';
        
        return `${this.product.validity.quantity} ${this.product.validity.unit.toLowerCase()}${unitSuffix}`;
    }

    toggleExpand() {
        this.isExpanded = !this.isExpanded;
    }

    onOrderClick() {
        this.snackbarService.info(`${this.product.name} has been ordered`);
    }
}
