export type SnackbarType = 'info' | 'error';
