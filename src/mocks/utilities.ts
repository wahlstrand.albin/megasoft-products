import { HttpResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

function toUrlSearchParams(urlWithParams: string): URLSearchParams {
    const queryString = urlWithParams.split('?')[1];
    const searchParams = new URLSearchParams(queryString);

    return searchParams;
}

function response(body?: any, status = 200): Observable<HttpResponse<any>> {
    const options: any = { status };

    if (body) {
        options.body = body;
    }

    const httpResponse = new HttpResponse(options);

    return of(httpResponse).pipe(delay(100));
}

export {
    toUrlSearchParams,
    response
};
