export enum ProductType {
    FIXED_VALUE_RECHARGE,
    RANGED_VALUE_RECHARGE,
    FIXED_VALUE_PIN_PURCHASE,
    RANGED_VALUE_PIN_PURCHASE,
    RANGED_VALUE_PAYMENT
}

export enum BenefitType {
    TALKTIME,
    DATA,
    SMS,
    PAYMENT,
    CREDITS
}

export interface ProductSearchQuery {
    type: ProductType;
    service_id: number;
    tags: string[];
    country_iso_code?: string;
    operator_id: number;
    region: string;
    benefit_types: BenefitType[];
    page: number;
    per_page: number;
}
