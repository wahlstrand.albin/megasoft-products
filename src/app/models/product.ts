import { Benefit } from './benefit';
import { Operator } from './operator';

export enum ProductType {
    FIXED_VALUE_RECHARGE = 'FIXED_VALUE_RECHARGE',
    RANGED_VALUE_RECHARGE = 'RANGED_VALUE_RECHARGE',
    FIXED_VALUE_PIN_PURCHASE = 'FIXED_VALUE_PIN_PURCHASE',
    RANGED_VALUE_PIN_PURCHASE = 'RANGED_VALUE_PIN_PURCHASE',
    RANGED_VALUE_PAYMENT = 'RANGED_VALUE_PAYMENT'
}

export interface ProductSource {
    amount: number;
    unit: string;
    unit_type: string;
}

export interface Validity {
    unit: string;
    quantity: number;
}

export interface Product {
    id: number;
    name: string;
    description: string;
    type: ProductType;
    validity: Validity;
    source: ProductSource;
    benefits: Benefit[];
    operator: Operator;
    tags: string[];
}