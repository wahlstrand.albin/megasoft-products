import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@msoft/modules/material/material.module';

import { ProductTypeComponent } from './product-type.component';
import { msoftProductTypePipe } from '@msoft/pipes/product-type.pipe';

@NgModule({
    declarations: [
        msoftProductTypePipe,
        ProductTypeComponent
    ],
    imports: [
        CommonModule,
        MaterialModule
    ],
    exports: [
        ProductTypeComponent
    ]
})
export class ProductTypeModule { }
