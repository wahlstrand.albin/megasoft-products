import { Injectable } from '@angular/core';

import { HttpInterceptor } from '@angular/common/http';
import { HttpEvent, HttpHandler, } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';

import { service } from './service';
import { response, toUrlSearchParams } from '../utilities';

@Injectable()
export class HttpMockProductRequestInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url === 'api/products' && request.method === 'GET') {
            const params = toUrlSearchParams(request.urlWithParams);
            const products = service.getProducts(params);

            return response(products);
        }

        return next.handle(request);
    }
}
