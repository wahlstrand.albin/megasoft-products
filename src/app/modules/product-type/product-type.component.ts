import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ProductType } from '@msoft/models/product';

@Component({
    selector: 'msoft-product-type',
    templateUrl: './product-type.component.html',
    styleUrls: ['./product-type.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductTypeComponent implements OnInit {
    imgsrc: string;
    @Input() productType: ProductType;

    ngOnInit(): void {
        switch (this.productType) {
            case ProductType.FIXED_VALUE_PIN_PURCHASE:
            case ProductType.RANGED_VALUE_PIN_PURCHASE:
                this.imgsrc = '/assets/images/voucher_icon.png';
                break;
            case ProductType.FIXED_VALUE_RECHARGE:
            case ProductType.RANGED_VALUE_RECHARGE:
                this.imgsrc = '/assets/images/fuel_icon.png';
                break;
            case ProductType.RANGED_VALUE_PAYMENT:
                this.imgsrc = '/assets/images/wallet_icon.png';
                break;
            default:
                this.imgsrc = '/assets/images/missing.png';
                break;
        }
    }
}
