import { Injectable } from '@angular/core';
import { FlatCountry } from './flat-country';

import { countries } from 'countries-list';
import alpha3s from 'countries-list/dist/countries2to3.json';

// I couldn't get alpha3s to behave in this version of typescript
// so I had to type it as an any

@Injectable({ providedIn: 'root' })
export class CountryService {
    countryList: FlatCountry[];

    getAllCountries(): FlatCountry[] {
        if (!this.countryList) {
            const countryKeys = Object.entries(countries);

            this.countryList = countryKeys.map(([key, value]) => {
                const countryData = value;

                return {
                    iso3: (alpha3s as any)[key],
                    name: countryData.name,
                    flag: countryData.emoji
                } as FlatCountry;
            }).sort((a, b) => a.name > b.name ? 1 : -1);
        }

        return this.countryList;
    }

    getCountryFlag(iso3: string): string | undefined {
        const countries = this.getAllCountries();

        return countries.find(x => x.iso3 === iso3)?.flag;
    }
}
