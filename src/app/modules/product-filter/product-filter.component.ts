import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { Subscription, tap } from 'rxjs';

import { CountryService } from '@msoft/services/country/country.service';
import { ProductFilterService } from '@msoft/services/product-filter/product-filter.service';

import { ProductSearchQuery } from '@msoft/models/product-search-query';
import { FlatCountry } from '@msoft/services/country/flat-country';

@Component({
    selector: 'msoft-product-filter',
    templateUrl: './product-filter.component.html',
    styleUrls: ['./product-filter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductFilterComponent implements OnInit, OnDestroy {
    private countrySubscription: Subscription;
    private tagsSubscription: Subscription;

    readonly maxTagsLimit = 3;
    tagsCount = 0;

    availableCountries: FlatCountry[];

    readonly tagSeparatorKeysCodes = [ENTER, COMMA] as const;

    searchQuery: ProductSearchQuery = {
        per_page: 10
    } as ProductSearchQuery;
    isLoading = false;

    formGroup: FormGroup;

    private get countryCtrl(): AbstractControl { return this.formGroup.get('country') ?? new FormControl(''); }
    private get tagsCtrl(): AbstractControl { return this.formGroup.get('tags') ?? new FormControl(''); }

    constructor(
        private countryService: CountryService,
        private productFilterService: ProductFilterService
    ) { }

    ngOnInit(): void {
        this.availableCountries = this.countryService.getAllCountries();

        // Set up input listeners
        this.formGroup = this.createFormGroup();
        this.searchQuery.tags = [];

        this.tagsSubscription = this.tagsCtrl.valueChanges.subscribe(value => {
            this.searchQuery.tags = value;
        });

        this.tagsCtrl.valueChanges.pipe(
            tap(value => {
                this.resetPage();

                this.tagsCount = value.length;

                this.searchQuery.tags = value;
                this.productFilterService.setFilter(this.searchQuery);
            })).subscribe();

        this.countrySubscription = this.countryCtrl.valueChanges.subscribe(value => {
            this.resetPage();

            if (value) {
                this.searchQuery.country_iso_code = value;
            } else {
                delete this.searchQuery.country_iso_code;
            }

            this.productFilterService.setFilter(this.searchQuery);
        });
    }

    ngOnDestroy(): void {
        if (this.tagsSubscription) {
            this.tagsSubscription.unsubscribe();
        }

        if (this.countrySubscription) {
            this.countrySubscription.unsubscribe();
        }
    }

    add(event: MatChipInputEvent): void {
        const value = (event.value || '').trim();

        if (value) {
            const uppercaseValue = value.toUpperCase();
            this.tagsCtrl.setValue([...this.tagsCtrl.value, uppercaseValue]);
        }

        event.chipInput!.clear();
    }

    remove(tag: string): void {
        const index = this.tagsCtrl.value.indexOf(tag);

        if (index >= 0) {
            this.searchQuery.tags.splice(index, 1);
            this.tagsCtrl.updateValueAndValidity();
        }
    }

    resetPage() {
        this.searchQuery.page = 1;
    }

    private createFormGroup(): FormGroup {
        const tagCtrl = new FormControl('');
        const countryCtrl = new FormControl('');

        return new FormGroup({
            tags: tagCtrl,
            country: countryCtrl
        });
    }
}
