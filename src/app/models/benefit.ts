export enum BenefitTypes {
    TALKTIME,
    DATA,
    SMS,
    PAYMENT,
    CREDITS
}

export interface BenefitAmount {
    base: number;
    promotion_bonus: number;
    total_excluding_tax: number;
}

export interface Benefit {
    additional_information: string;
    amount: BenefitAmount;
    type: string;
    unit: string;
    unit_type: string;
}