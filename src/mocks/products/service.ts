import { Product } from '@msoft/models/product';

import products from './products.json';

export const service = {
    getProducts
};

function getProducts(params: URLSearchParams): Product[] {
    const page = params.get('page');
    const tags = params.getAll('tags');
    const country = params.get('country_iso_code');

    let filteredProducts = products;

    // Test when we get empty results back
    if (page && Number.parseInt(page) > 3) {
        return [];
    }

    if (tags && tags.length || country) {
        filteredProducts = [];

        for (const product of products) {
            const hasTag = product.tags.some(x => tags.includes(x));
            const hasCountry = product.operator?.country.iso_code === country;

            if (hasTag || hasCountry) {
                filteredProducts.push(product);
            }
        }
    }

    return JSON.parse(JSON.stringify(filteredProducts)) as Product[];
}
