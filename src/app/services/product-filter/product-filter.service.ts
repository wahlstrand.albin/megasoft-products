import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { ProductSearchQuery } from '@msoft/models/product-search-query';

@Injectable()
export class ProductFilterService {
    private filter$ = new BehaviorSubject<ProductSearchQuery>({
        page: 1,
        per_page: 10
    } as ProductSearchQuery);

    getFilter$(): Observable<ProductSearchQuery> {
        return this.filter$.asObservable();
    }

    setFilter(searchQuery: ProductSearchQuery) {
        this.filter$.next(searchQuery);
    }
}
