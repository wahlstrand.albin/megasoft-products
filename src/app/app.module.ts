import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ENVIRONMENT_PROVIDERS } from './../environments/environment';

import { CountryService } from './services/country/country.service';
import { ProductFilterService } from './services/product-filter/product-filter.service';
import { SnackbarService } from '@msoft/services/snackbar/snackbar.service';

import { ProductsService } from '@msoft/api/products.service';

import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from '@msoft/pages/home/home.module';
import { MaterialModule } from './modules/material/material.module';
import { SharedModule } from '@msoft/modules/shared/shared.module';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        HttpClientModule,
        MaterialModule,
        SharedModule,

        // Pages
        HomeModule
    ],
    providers: [
        // Services
        CountryService,
        ProductFilterService,
        SnackbarService,

        // API Services
        ProductsService,

        ENVIRONMENT_PROVIDERS
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
