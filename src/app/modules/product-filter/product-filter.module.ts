import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '@msoft/modules/material/material.module';

import { ProductFilterComponent } from './product-filter.component';

@NgModule({
    declarations: [
        ProductFilterComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule
    ],
    exports: [
        ProductFilterComponent
    ]
})
export class ProductFilterModule { }
