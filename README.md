# Megasoft

This is the POC for Megasofts products discovery. It is written in Angular V13.

## Requirements

    - node 14.17.0 or higher
    - yarn 1.22.0 or higher

## Running the UI

The application runs with mocks based on a REST api provided by Megasoft.
Start by running `yarn` to install the packages.

If you use `yarn start` it will start the application.

Navigate to `http://localhost:4200/` and the app will direct you to the products page.

## Testing

Since this is a technical assessment, I have provided an example of tests in the `benefit.component.spec.ts` file to show how to approach testing.

- For running unit tests: `yarn test`
- For linting your code: `yarn lint`

## Building container

To build the container, run the command `yarn && yarn build` to create the dist folder, then use the command `yarn docker:build` to build a docker image.

## Running container

Provide `API_ENDPOINT`, `API_SECRET` and `API_KEY` to the `docker-compose.yml` file, then run `docker-compose up` to start the container, which will then be accessible from `http://localhost:4205/`.

### Assets

All the images are free from [Icon-icons](https://icon-icons.com/).

### Suggested improvements

Here are some improvement suggestions:

    - Loading handling
    - Different icon for fixed and ranged value recharge and pin purchase
    - Additional filters
    - Better way of handling the headlines/"columns"
    - Handle mobile/tablet screen sizes
    - Add pagination with total number of pages and products (Requires API support)
    - Show product page on row click
    - Add operator logotypes (Requires API support)
