import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'msoftProductType' })
export class msoftProductTypePipe implements PipeTransform {
    transform(value: string): string {
        if (!value) {
            return '';
        }

        const result = value.replace(/_/g,' ');
        const finalResult = result.charAt(0).toUpperCase() + result.slice(1).toLowerCase();

        return finalResult;
    }
}
