import { Routes } from '@angular/router';

import { HomeComponent } from './home.component';

export const homeRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'products', loadChildren: () => import('@msoft/pages/products/products.module').then(module => module.ProductsModule)
            },
            {
                path: '', redirectTo: '/products', pathMatch: 'full'
            }
        ]
    }
];
