import { Component } from '@angular/core';

@Component({
    selector: 'msoft-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {}
