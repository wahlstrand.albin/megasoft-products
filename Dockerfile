FROM nginx:1.21.5-alpine

COPY docker/nginx.conf.template /app/
COPY --chmod=755 docker/entrypoint.sh /app/
COPY dist/ /web/

HEALTHCHECK --timeout=5s CMD wget --no-verbose --tries=1 --spider http://localhost/healthcheck || exit 1

ENTRYPOINT /app/entrypoint.sh
