import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HttpMockProductRequestInterceptor } from '@msoft/mocks/products/interceptor';

export const environment = {
    production: false,
    development: true
};

export const ENVIRONMENT_PROVIDERS = [
    { provide: HTTP_INTERCEPTORS, useClass: HttpMockProductRequestInterceptor, multi: true }
];
