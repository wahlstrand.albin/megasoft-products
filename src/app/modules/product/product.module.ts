import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@msoft/modules/material/material.module';

import { ProductComponent } from './product.component';
import { BenefitModule } from '../benefit/benefit.module';
import { ProductTypeModule } from '../product-type/product-type.module';

@NgModule({
    declarations: [
        ProductComponent
    ],
    imports: [
        BenefitModule,
        CommonModule,
        MaterialModule,
        ProductTypeModule
    ],
    exports: [
        ProductComponent
    ]
})
export class ProductModule { }
