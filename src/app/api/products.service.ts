import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Product } from '@msoft/models/product';

@Injectable()
export class ProductsService {
    constructor(protected httpClient: HttpClient) { }

    public getProducts(searchRequest = {}): Observable<Product[]> {
        const endpoint = 'api/products';

        return this.httpClient.get<Product[]>(endpoint, { params: searchRequest });
    }
}
