import { Component, OnDestroy, OnInit } from '@angular/core';
import { catchError, of, Subject, Subscription, switchMap, tap } from 'rxjs';

import { ProductsService } from '@msoft/api/products.service';

import { Product } from '@msoft/models/product';
import { ProductSearchQuery } from '@msoft/models/product-search-query';
import { SnackbarService } from '@msoft/services/snackbar/snackbar.service';
import { ProductFilterService } from '@msoft/services/product-filter/product-filter.service';

@Component({
    selector: 'msoft-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
    filterSubscription$: Subscription;
    pageSubscription$: Subscription;

    products: Product[] = [];

    trigger$ = new Subject<void>();
    noMoreProducts = false;

    searchRequest: ProductSearchQuery = {
        page: 1,
        per_page: 10
    } as ProductSearchQuery;

    constructor(
        private snackbarService: SnackbarService,
        private productsService: ProductsService,
        private productFilterService: ProductFilterService
    ) {}

    ngOnInit() {
        // Filter changes
        this.filterSubscription$ = this.productFilterService.getFilter$().pipe(
            tap(filter => { this.searchRequest = filter; }),
            switchMap(() =>
                this.fetchNewProducts$()
            )
        ).subscribe();

        // Page changes
        this.pageSubscription$ = this.trigger$.pipe(
            switchMap(
                () => this.getProductsFromNewPage$()
            )
        ).subscribe();
    }

    ngOnDestroy() {
        if (this.filterSubscription$) {
            this.filterSubscription$.unsubscribe();
        }

        if (this.pageSubscription$) {
            this.filterSubscription$.unsubscribe();
        }
    }

    onLoadMore() {
        this.searchRequest.page++;
        this.trigger$.next();
    }

    // Used when filtering
    private fetchNewProducts$() {
        return this.getProducts$().pipe(
            tap(products => {
                this.products = products;
            })
        );
    }

    // Used when changing page
    private getProductsFromNewPage$() {
        return this.getProducts$().pipe(
            tap(products => {
                this.products.push(...products);
            })
        );
    }

    private getProducts$() {
        return this.productsService.getProducts(this.searchRequest).pipe(
            tap(products => {
                if (!products?.length) {
                    this.noMoreProducts = true;
                } else {
                    this.noMoreProducts = false;
                }
            }),
            catchError(err => {
                if (err.error?.errors) {
                    for (const error of err.error.errors) {
                        this.snackbarService.error(error.message);
                    }

                    return of([]);
                }

                this.snackbarService.error('There was an unexpected error while fetching products');

                return of([]);
            })
        );
    }
}
