import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '@msoft/modules/material/material.module';
import { ToolbarModule } from '@msoft/modules/toolbar/toolbar.module';

import { HomeComponent } from './home.component';

import { homeRoutes } from './home.routes';

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forChild(homeRoutes),
        ToolbarModule
    ]
})
export class HomeModule { }
