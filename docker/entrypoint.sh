#!/bin/sh

# Ensures container will only start with the required environment variables
[ -z "$API_ENDPOINT" ] && echo "API_ENDPOINT needs to be added to the environment" && exit 1;
[ -z "$API_KEY" ] && echo "API_KEY needs to be added to the environment" && exit 1;
[ -z "$API_SECRET" ] && echo "API_SECRET needs to be added to the environment" && exit 1;

# Removes default config file
rm /etc/nginx/conf.d/default.conf

# Base64 encodes username and password for basic auth
#   -n   = no line break at end of line
#   -w 0 = disables line wrapping (default is 76 chars)
export API_TOKEN=$(echo -n "$API_KEY:$API_SECRET" | base64 -w 0)

# Replaces environment variables in config template and stores it in the configuration path
envsubst '\$API_TOKEN \$API_ENDPOINT' < /app/nginx.conf.template > /etc/nginx/conf.d/msoft.conf

# Setting entrypoint
exec nginx -g 'daemon off;'