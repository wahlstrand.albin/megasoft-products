import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Benefit } from '@msoft/models/benefit';

@Component({
    selector: 'msoft-benefit',
    templateUrl: './benefit.component.html',
    styleUrls: ['./benefit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BenefitComponent implements OnInit {
    displayAmount = true;

    @Input() benefit: Benefit;

    ngOnInit(): void {
        this.displayAmount = this.shouldDisplayAmount();
    }

    private shouldDisplayAmount() {
        if (this.benefit?.amount?.base !== -1) {
            return true;
        }

        return false;
    }
}
