import { Benefit } from '@msoft/models/benefit';
import { BenefitComponent } from './benefit.component';

// This is an example of how testing would be done. 
// Considering this is just a POC, I have decided to show a single example as to how this is done

describe('Benefit', () => {
    let component: BenefitComponent;

    beforeEach(() => {
        component = new BenefitComponent();
    });

    describe('when initializing', () => {
        describe('with benefit base amount 1', () => {
            const baseAmount = 1;
            beforeEach(() => {
                component.benefit = {
                    amount: {
                        base: baseAmount,
                        promotion_bonus: 0,
                        total_excluding_tax: 0
                    }
                } as Benefit;

                component.ngOnInit();
            });

            it('it should set the displayAmount to true', () => {
                expect(component.displayAmount).toBeTrue();
            });
        });

        describe('with benefit base amount -1', () => {
            const baseAmount = -1;
            beforeEach(() => {
                component.benefit = {
                    amount: {
                        base: baseAmount,
                        promotion_bonus: 0,
                        total_excluding_tax: 0
                    }
                } as Benefit;

                component.ngOnInit();
            });

            it('it should set the displayAmount to false', () => {
                expect(component.displayAmount).toBeFalse();
            });
        });
    });
});
