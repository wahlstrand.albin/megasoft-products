import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material/material.module';

import { SnackbarComponent } from '@msoft/services/snackbar/snackbar.component';

@NgModule({
    declarations: [
        SnackbarComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule
    ]
})
export class SharedModule { }
