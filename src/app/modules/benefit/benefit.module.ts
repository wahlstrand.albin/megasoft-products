import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenefitComponent } from './benefit.component';

@NgModule({
    declarations: [
        BenefitComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        BenefitComponent
    ]
})
export class BenefitModule { }
