import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';

import { SnackbarComponent } from './snackbar.component';
import { SnackbarType } from './snackbar.type';

interface SnackbarItem {
    message: string;
    icon: string;
    type: SnackbarType;
}

@Injectable()
export class SnackbarService {
    queue: SnackbarItem[] = [];
    currentSnackbarDisplaying: MatSnackBarRef<SnackbarComponent> | null = null;

    constructor(
        private snackBar: MatSnackBar
    ) { }

    public error(message: string): void {
        this.queue.push({ message, icon: 'warning', type: 'error' });
        this.startSnackbarQueue();
    }

    public info(message: string): void {
        this.queue.push({ message, icon: 'info', type: 'info' });
        this.startSnackbarQueue();
    }

    private startSnackbarQueue() {
        if (this.currentSnackbarDisplaying === null) {
            const item = this.queue.pop();

            if (item) {
                this.openSnackbar(item);
            }
        }
    }

    private openSnackbar(snackbarItem: SnackbarItem): void {
        const options = this.getSnackbarOptions(snackbarItem);

        this.currentSnackbarDisplaying = this.snackBar.openFromComponent(SnackbarComponent, options);
        this.currentSnackbarDisplaying.afterDismissed().subscribe(() => this.onAfterDismissed());
    }

    private getSnackbarOptions(snackbarItem: SnackbarItem): MatSnackBarConfig {
        return {
            duration: 4000,
            horizontalPosition: 'center',
            data: {
                message: snackbarItem.message,
                icon: snackbarItem.icon,
                type: snackbarItem.type,
                queued: this.queue
            }
        };
    }

    private onAfterDismissed() {
        this.currentSnackbarDisplaying = null;
        const nextItem = this.queue.pop();

        if (nextItem) {
            this.openSnackbar(nextItem);
        }
    }
}
