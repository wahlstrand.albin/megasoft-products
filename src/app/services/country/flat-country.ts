export interface FlatCountry {
    iso3: string;
    name: string;
    flag: string;
}
