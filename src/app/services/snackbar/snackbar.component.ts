import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { SnackbarType } from './snackbar.type';

@Component({
    selector: 'msoft-snackbar',
    templateUrl: './snackbar.component.html',
    styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {
    @HostBinding('class') type: SnackbarType;

    message: string;
    icon: string;
    queued: [];

    get queueLength() { return this.queued.length; }

    constructor(
        @Inject(MAT_SNACK_BAR_DATA) public data: any
    ) {}

    ngOnInit() {
        this.message = this.data.message;
        this.type = this.data.type;
        this.icon = this.data.icon;
        this.queued = this.data.queued;
    }
}
